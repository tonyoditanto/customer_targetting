from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import googlemaps
import pandas as pd
import json
import requests
from time import sleep
# kodokterbang
app = Flask(__name__)

# POST /customer_targetting data: {latitude, longitude, keyword, type[], radius}


@app.route('/customer_targetting', methods=['POST'])
def grab_customer():
    customer_targetting = []
    request_data = request.get_json()
    lat_ori = request_data['latitude']
    long_ori = request_data['longitude']
    keyword_user = request_data['keyword']
    types_user = request_data['type']
    radius = request_data['radius']

    data_scraps = scrapping(
        lat_ori, long_ori, keyword_user, types_user, radius)
    if len(data_scraps) > 0:
        for data in data_scraps:
            new_data = {
                'customer_name': data['customer_name'],
                'customer_address': data['customer_address'],
                'customer_type': data['customer_type'],
                'keyword': data['keyword'],
                'radius': data['radius'],
                'place_id': data['place_id'],
                'latitude_origin': data['latitude_origin'],
                'longitude_origin': data['longitude_origin'],
                'latitude_destination': data['latitude_destination'],
                'longitude_destination': data['longitude_destination'],
                'phone_number': data['phone_number'],
                'distance_driving': data['distance_driving'],
            }
            customer_targetting.append(new_data)
        """filter_data = []
        for customer in customer_targetting:
            new_data = {

            }"""
        return jsonify(customer_targetting), 200
    else:
        return {"message": "customer tidak dalam jangkauan"}


def scrapping(latitude, longitude, keyword, type, radius):
    lat_ori = latitude
    long_ori = longitude
    keyword_user = keyword
    types_user = type
    radius = radius
    data_cust = {}
    #token, latitude, longitude, name, place_id, types_places, vicinity = [],[],[],[],[],[], []

    apik = 'AIzaSyDoC4Ei0ilIETerxUq7KkS3WYtan8241dA'
    urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&key={}&type={}&keyword={}'.format(
        lat_ori, long_ori, radius, apik, types_user, keyword_user)
    r = requests.get(urls)
    data_cust['0'] = r.json()

    """
    /////////////////////////////////////////////////////////////////////////////

    CODE FOR NEXT PAGE TOKEN

    /////////////////////////////////////////////////////////////////////////////
    """

    for number in range(10):

        content = str(number)
        if 'next_page_token' in data_cust[content].keys():
            sleep(5)
            pagetoken = data_cust[content]['next_page_token']
            apik = 'AIzaSyDoC4Ei0ilIETerxUq7KkS3WYtan8241dA'
            urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&type={}&keyword={}&key={}{pagetoken}'.format(
                lat_ori, long_ori, radius, types_user, keyword_user, apik, pagetoken="&pagetoken="+pagetoken if pagetoken else "")
            r = requests.get(urls)
            get = requests.post(urls)
            print(get)
            new_id = str(number+1)
            data_cust[new_id] = r.json()
        else:
            print("Done")
            break

    latitude, longitude, name, place_id, types_places, vicinity = [], [], [], [], [], []
    for i in range(number+1):
        content = str(i)
        for numbers in range(len(data_cust[content]['results'])):
            latitude.append(data_cust[content]['results']
                            [numbers]['geometry']['location']['lat'])
            longitude.append(data_cust[content]['results']
                             [numbers]['geometry']['location']['lng'])
            name.append(data_cust[content]['results'][numbers]['name'])
            place_id.append(data_cust[content]['results'][numbers]['place_id'])
            types_places.append(
                data_cust[content]['results'][numbers]['types'][0])
            vicinity.append(data_cust[content]['results'][numbers]['vicinity'])

    datacustype = pd.DataFrame({'customer_name': name, 'customer_type': types_places, 'place_id': place_id,
                                'keyword': keyword_user, 'radius': radius, 'latitude_origin': lat_ori, 'longitude_origin': long_ori, 'latitude_destination': latitude,
                                'longitude_destination': longitude})
    datacustype

    """
    /////////////////////////////////////////////////////////////////////////////

    PHONE NUMBER

    /////////////////////////////////////////////////////////////////////////////
    """

    data_number = {}
    for number in datacustype['place_id'].values:
        apik = 'AIzaSyDoC4Ei0ilIETerxUq7KkS3WYtan8241dA'
        urls = 'https://maps.googleapis.com/maps/api/place/details/json?place_id={}&fields=name,formatted_address,rating,formatted_phone_number&key={}'.format(
            number, apik)
        r = requests.get(urls)
        data_number[number] = r.json()

    data_number

    datanumb = pd.DataFrame.from_dict(data_number).T.reset_index()
    datanumb.columns = ['place_id', 'html_attributions', 'result', 'status']
    datanumb

    name, phone, alamat = [], [], []

    for number in range(len(datanumb)):
        if datanumb['status'][number] == 'NOT_FOUND':
            name.append('Unknown')
            phone.append('Not Available')
            alamat.append('-')
        else:
            name.append(datanumb['result'][number]['name'])
            alamat.append(datanumb['result'][number]['formatted_address'])
            if 'formatted_phone_number' in (datanumb['result'][number].keys()):
                phone.append(datanumb['result'][number]
                             ['formatted_phone_number'])
            else:
                phone.append('Not Available')

    datanumb2 = pd.DataFrame(
        {'customer_name': name, 'customer_address': alamat, 'phone_number': phone})
    datanumb2['place_id'] = datanumb['place_id']
    datanumb2

    """
    /////////////////////////////////////////////////////////////////////////////

    DATA MERGE

    /////////////////////////////////////////////////////////////////////////////
    """

    datamerge = datacustype.merge(datanumb2, how='left', on='place_id')
    datamerge

    """
    /////////////////////////////////////////////////////////////////////////////

    DUMMY

    /////////////////////////////////////////////////////////////////////////////
    """

    datadummy = datamerge.copy()
    datadummy

    datadummydrop = datadummy.drop(['customer_name_y'], axis=1)
    datadummydrop.rename(
        columns={'customer_name_x': 'customer_name'}, inplace=True)
    datadummydrop2 = datadummydrop[['customer_name', 'customer_address', 'customer_type', 'keyword', 'radius',
                                    'place_id', 'latitude_origin', 'longitude_origin', 'latitude_destination', 'longitude_destination', 'phone_number']]
    datadummydrop2

    """
    /////////////////////////////////////////////////////////////////////////////

    DISTANCE MATRIX

    /////////////////////////////////////////////////////////////////////////////
    """

    API_key = 'AIzaSyDoC4Ei0ilIETerxUq7KkS3WYtan8241dA'  # enter Google Maps API key
    gmaps = googlemaps.Client(key=API_key)

    distancedrive, distancewalks = [], []

    # Loop through each row in the data frame using pairwise
    for number in range(datadummydrop2.shape[0]):
        # Assign latitude and longitude as origin/departure points
        LatOrigin = datadummydrop2['latitude_origin'][number]
        LongOrigin = datadummydrop2['longitude_origin'][number]
        origins = (LatOrigin, LongOrigin)

        # Assign latitude and longitude from the next row as the destination point
        # Save value as lat
        LatDest = datadummydrop2['latitude_destination'][number]
        # Save value as lat
        LongDest = datadummydrop2['longitude_destination'][number]
        destination = (LatDest, LongDest)

        # pass origin and destination variables to distance_matrix function# output in meters
        result = gmaps.distance_matrix(origins, destination, mode='driving', avoid='tolls',
                                       units='metric', departure_time=1703981100)["rows"][0]["elements"][0]["distance"]["value"]
    # 1703981100    #1606867500
        # append result to list
        distancedrive.append(result)

    datadummydrop2['distance_driving'] = distancedrive
    datadummydrop3 = datadummydrop2.sort_values(
        by=['distance_driving'], ascending=True, ignore_index=True)
    datadummydrop3

    datas = []
    for i in datadummydrop3.index:
        """datas['customer_name'] = datadummydrop3['customer_name'][i]
        datas['customer_address'] = datadummydrop3['customer_address'][i]
        datas['customer_type'] = datadummydrop3['customer_type'][i]
        datas['keyword'] = datadummydrop3['keyword'][i]
        datas['radius'] = datadummydrop3['radius'][i]
        datas['place_id'] = datadummydrop3['place_id'][i]
        datas['latitude_origin'] = datadummydrop3['latitude_origin'][i]
        datas['longitude_origin'] = datadummydrop3['longitude_origin'][i]
        datas['latitude_destination'] = datadummydrop3['latitude_destination'][i]
        datas['longitude_destination'] = datadummydrop3['longitude_destination'][i]
        datas['phone_number'] = datadummydrop3['phone_number'][i]
        datas['distance_driving'] = datadummydrop3['distance_driving'][i]"""
        new_data = {
            'customer_name': datadummydrop3['customer_name'][i],
            'customer_address': datadummydrop3['customer_address'][i],
            'customer_type': datadummydrop3['customer_type'][i],
            'keyword': datadummydrop3['keyword'][i],
            'radius': float(datadummydrop3['radius'][i]),
            'place_id': datadummydrop3['place_id'][i],
            'latitude_origin': float(datadummydrop3['latitude_origin'][i]),
            'longitude_origin': float(datadummydrop3['longitude_origin'][i]),
            'latitude_destination': float(datadummydrop3['latitude_destination'][i]),
            'longitude_destination': float(datadummydrop3['longitude_destination'][i]),
            'phone_number': str(datadummydrop3['phone_number'][i]),
            'distance_driving': float(datadummydrop3['distance_driving'][i]),
        }
        # print(new_data['customer_name'])
        datas.append(new_data)

    print("Data berhasil di upload")
    return datas


app.run(port=5000, host='0.0.0.0')
