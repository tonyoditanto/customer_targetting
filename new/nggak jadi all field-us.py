#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import json
import requests
from time import sleep


# In[2]:


lat_ori = input('Masukkan lat asal:')
long_ori = input('Masukkan long asal:')
radius = input('Masukkan radius(dalam meter):')
types_user = input('Masukkan types:')
keyword_user = input('Masukkan keyword:')


# In[ ]:


#indo:
#lat origin :#-6.2097
#long origin:106.90166
#US:
#lat origin :40.712772
#long origin:-74.006058


# # coding first page

# In[3]:


data_cust={}

apik = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'
urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&key={}&type={}&keyword={}&fields=business_status,formatted_address,geometry,icon,name,photos,place_id,plus_code,opening_hours,types,price_level,rating,user_ratings_total'.format(lat_ori,long_ori,radius,apik,types_user,keyword_user)
r = requests.get(urls)
data_cust['0'] = r.json()


# # code for next page token

# In[4]:


for number in range(10):
    
    content = str(number)
    if 'next_page_token' in data_cust[content].keys():
        sleep(5)
        pagetoken = data_cust[content]['next_page_token']
        apik = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'
        urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&type={}&keyword={}&fields=business_status,formatted_address,geometry,icon,name,photos,place_id,plus_code,opening_hours,types,price_level,rating,user_ratings_total&key={}{pagetoken}'.format(lat_ori,long_ori,radius,types_user,keyword_user,apik, pagetoken = "&pagetoken="+pagetoken if pagetoken else "")
        r = requests.get(urls)
        get = requests.post(urls)
        print(get)
        new_id = str(number+1)
        data_cust[new_id] = r.json()
    else:
        print("Done")
        break


# In[ ]:


data_cust['0']['results'][0]


# In[5]:


business_status,latitude_loc, longitude_loc,latitude_north, longitude_north,latitude_south, longitude_south, name, place_id, types_places, vicinity = [],[],[],[],[],[],[],[],[],[],[]
opening,price_level,icon,comp,globc,rating,reference,scope,types2,types3,types4,user_rat,photo_height,photo_width,html_attributions,photo_reference=[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]

for i in range(number+1):
    content = str(i)
    for numbers in range(len(data_cust[content]['results'])):
        try: 
            latitude_loc.append(data_cust[content]['results'][numbers]['geometry']['location']['lat'])
        except:
            latitude_loc.append('-')
        try:
            longitude_loc.append(data_cust[content]['results'][numbers]['geometry']['location']['lng'])
        except:
            longitude_loc.append('-')
        try:
            name.append(data_cust[content]['results'][numbers]['name'])
        except:
            name.append('Unknown')
        try:
            place_id.append(data_cust[content]['results'][numbers]['place_id'])
        except:
            place_id.append('-')
        try:
            types_places.append(data_cust[content]['results'][numbers]['types'][0])
        except:
            types_places.append('-')
        try:
            types2.append(data_cust[content]['results'][numbers]['types'][1])
        except:
            types2.append('-')
        try:
            types3.append(data_cust[content]['results'][numbers]['types'][2])
        except:
            types3.append('-')
        try:
            types4.append(data_cust[content]['results'][numbers]['types'][3])
        except:
            types4.append('-')
        try:
            vicinity.append(data_cust[content]['results'][numbers]['vicinity'])
        except:
            vicinity.append('-')
        try:
            business_status.append(data_cust[content]['results'][numbers]['business_status'])
        except:
            business_status.append('-')
        try:
            latitude_north.append(data_cust[content]['results'][numbers]['geometry']['viewport']['northeast']['lat'])
        except:
            latitude_north.append('-')
        try:
            longitude_north.append(data_cust[content]['results'][numbers]['geometry']['viewport']['northeast']['lng'])
        except:
            longitude_north.append('-')
        try:
            latitude_south.append(data_cust[content]['results'][numbers]['geometry']['viewport']['southwest']['lat'])
        except:
            latitude_south.append('-')
        try:
            longitude_south.append(data_cust[content]['results'][numbers]['geometry']['viewport']['southwest']['lng'])
        except:
            longitude_south.append('-')
        try:
            icon.append(data_cust[content]['results'][numbers]['icon'])
        except:
            icon.append('-')
        try:
            comp.append(data_cust[content]['results'][numbers]['plus_code']['compound_code'])
        except:
            comp.append('-')
        try:
            globc.append(data_cust[content]['results'][numbers]['plus_code']['global_code'])
        except:
            globc.append('-')
        try:
            rating.append(data_cust[content]['results'][numbers]['rating'])
        except:
            rating.append('-')
        try:
            reference.append(data_cust[content]['results'][numbers]['reference'])
        except:
            reference.append('-')
        try:
            scope.append(data_cust[content]['results'][numbers]['scope'])
        except:
            scope.append('-')
        try:
            user_rat.append(data_cust[content]['results'][numbers]['user_ratings_total'])
        except:
            user_rat.append('-')
        try:
            opening.append(data_cust[content]['results'][numbers]['opening_hours']['open_now'])
        except:
            opening.append('-')
        try:
            photo_height.append(data_cust[content]['results'][numbers]['photos'][0]['height'])
        except:
            photo_height.append('-')
        try:
            photo_width.append(data_cust[content]['results'][numbers]['photos'][0]['width'])
        except:
            photo_width.append('-')
        try:
            html_attributions.append(data_cust[content]['results'][numbers]['photos'][0]['html_attributions'][0])
        except:
            html_attributions.append('-')
        try:
            photo_reference.append(data_cust[content]['results'][numbers]['photos'][0]['photo_reference'])
        except:
            photo_reference.append('-')
        try:
            price_level.append(data_cust[content]['results'][numbers]['price_level'])
        except:
            price_level.append('-')


# In[6]:


print('panjang list business_status: ', len(business_status))
print('panjang list latitude_loc: ', len(latitude_loc))
print('panjang list longitude_loc: ', len(longitude_loc))
print('panjang list latitude_north: ', len(latitude_north))
print('panjang list longitude_north: ', len(longitude_north))
print('panjang list latitude_south: ', len(latitude_south))
print('panjang list longitude_south: ', len(longitude_south))
print('panjang list name: ', len(name))
print('panjang list place_id: ', len(place_id))
print('panjang list address: ', len(vicinity))
print('panjang list icon: ', len(icon))
print('panjang list opening_hours: ', len(opening))
print('panjang list comp: ', len(comp))
print('panjang list globc: ', len(globc))
print('panjang list rating: ', len(rating))
print('panjang list reference: ', len(reference))
print('panjang list scope: ', len(scope))
print('panjang list types2: ', len(types2))
print('panjang list types3: ', len(types3))
print('panjang list types4: ', len(types4))
print('panjang list user_rating: ', len(user_rat))
print('panjang list photo_height: ', len(photo_height))
print('panjang list photo_width: ', len(photo_width))
print('panjang list html_attributions: ', len(html_attributions))
print('panjang list photo_reference: ', len(photo_reference))
print('panjang list price_level: ', len(price_level))


# In[7]:


datacustypes = pd.DataFrame({'business_status':business_status,'customer_name':name,'customer_address':vicinity,'customer_type':types_places,'types_places2':types2,'types_places3':types3,'types_places4':types4,
                             'place_id':place_id,'keyword':keyword_user,'radius':radius,'latitude_origin':lat_ori,'longitude_origin':long_ori,'latitude_destination':latitude_loc,
                            'longitude_destination':longitude_loc,'latitude_north':latitude_north,'longitude_north':longitude_north,'latitude_south':latitude_south,'longitude_south':longitude_south,'icon':icon,
                             'photo_height':photo_height,'photo_width':photo_width,'html_attributions':html_attributions,'photo_reference':photo_reference,
                             'reference':reference,'scope':scope,'opening_hours':opening,'compound_code':comp,'global_code':globc,'price_level':price_level,'rating':rating,'user_rating':user_rat})
pd.set_option('display.max_columns',40)
datacustypes


# # filter business_status = 'operational'

# In[8]:


datacustype = datacustypes.loc[datacustypes['business_status']=='OPERATIONAL']
datacustype


# # phone numb

# In[9]:


data_number = {}
for number in datacustype['place_id'].values:
    apik = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'
    urls = 'https://maps.googleapis.com/maps/api/place/details/json?place_id={}&fields=name,formatted_phone_number,international_phone_number,website&key={}'.format(number,apik)
    r = requests.get(urls)
    data_number[number] = r.json()


# In[10]:


datanumb = pd.DataFrame.from_dict(data_number).T.reset_index()
datanumb.columns = ['place_id','html_attributions','result','status']
datanumb


# In[ ]:


datanumb['result'][0]


# In[11]:


name,phone,intphonenumb,web=[],[],[],[]


for number in range(len(datanumb)):
    if datanumb['status'][number] == 'NOT_FOUND':
        name.append('Unknown')
        phone.append(0)
    else:
        try:
            name.append(datanumb['result'][number]['name'])
        except:
            name.append('Unknown')
        try:
            intphonenumb.append(datanumb['result'][number]['international_phone_number'])
        except:
            intphonenumb.append(0)
        try:
            web.append(datanumb['result'][number]['website'])
        except:
            web.append('-')
        if 'formatted_phone_number' in (datanumb['result'][number].keys()):
            phone.append(datanumb['result'][number]['formatted_phone_number'])
        else:
            phone.append(0)


# In[12]:


datanumb2 = pd.DataFrame({'customer_name':name,'website':web,'phone_number':phone,
                          'international_phone_numb':intphonenumb})
datanumb2['place_id'] = datanumb['place_id']
pd.set_option('display.max_columns',100)
datanumb2


# # merge

# In[13]:


datamerge=datacustype.merge(datanumb2, how='left', on='place_id')
datamerge


# # dummy

# In[14]:


datadummy=datamerge.copy()
datadummy


# In[15]:


datadummydrop=datadummy.drop(['customer_name_y'], axis = 1)
datadummydrop.rename(columns={'customer_name_x': 'customer_name'}, inplace=True)
datadummydrop


# # distance matrix

# In[16]:


import pandas as pd
import googlemaps


# In[17]:


API_key = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'#enter Google Maps API key
gmaps = googlemaps.Client(key=API_key)


# In[18]:


distancedrive = []

# Loop through each row in the data frame using pairwise
for number in range(datadummydrop.shape[0]):
      #Assign latitude and longitude as origin/departure points
    LatOrigin = datadummydrop['latitude_origin'][number]
    LongOrigin = datadummydrop['longitude_origin'][number]
    origins = (LatOrigin,LongOrigin)

      #Assign latitude and longitude from the next row as the destination point
    LatDest = datadummydrop['latitude_destination'][number]  # Save value as lat
    LongDest = datadummydrop['longitude_destination'][number] # Save value as lat
    destination = (LatDest,LongDest)
    

      #pass origin and destination variables to distance_matrix function# output in meters
    result = gmaps.distance_matrix(origins, destination, mode='driving',avoid='tolls',units='metric',traffic_model='optimistic',departure_time=1703981100)["rows"][0]["elements"][0]["distance"]["value"]
 #1703981100    #1606867500 
      #append result to list
    distancedrive.append(result)


# In[19]:


datadummydrop['Distance_to_Infra(m)']=distancedrive
datadummydrop3=datadummydrop.sort_values(by=['Distance_to_Infra(m)'],ascending=True,ignore_index=True)
datadummydrop3


# In[ ]:


datadummydrop3.shape


# # dataframe to postgre

# In[ ]:


import psycopg2

database = psycopg2.connect (database = "coba", 
                             user="postgres", 
                             password="kasihtaunggakya?", 
                             host="localhost")

cursor = database.cursor()

for i in datadummydrop3.index:
    c1 = datadummydrop3['customer_name'][i]
    c2 = datadummydrop3['customer_address'][i]
    c3 = datadummydrop3['customer_type'][i]
    c4 = datadummydrop3['keyword'][i]
    c5 = datadummydrop3['radius'][i]
    c6 = datadummydrop3['place_id'][i]
    c7 = datadummydrop3['latitude_origin'][i]
    c8 = datadummydrop3['longitude_origin'][i]
    c9 = datadummydrop3['latitude_destination'][i]
    c10 = datadummydrop3['longitude_destination'][i]
    c11 = datadummydrop3['phone_number'][i]
    c12 = datadummydrop3['Distance_to_Infra_driving'][i]
    c13 = datadummydrop3['Distance_to_Infra_walking'][i]
    query = """
    Insert into sukses3(customer_name, customer_address, customer_type, keyword, radius, place_id, latitude_origin, longitude_origin, latitude_destination, longitude_destination, phone_number, Distance_to_Infra_driving, Distance_to_Infra_walking) VALUES('%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,'%s',%s,%s);
    """ % (c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13)
    cursor.execute(query)
cursor.close()
    
database.commit()
database.close()

print ("Data berhasil di upload")

