#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import json
import requests
from time import sleep


# In[2]:


lat_ori = input('Masukkan lat asal:')
long_ori = input('Masukkan long asal:')
radius = input('Masukkan radius(dalam meter):')
types_user = input('Masukkan types:')
keyword_user = input('Masukkan keyword:')
#-6.2097
#106.90166


# In[ ]:


40.712772
Masukkan long asal:-74.006058


# # coding first page

# In[3]:


data_cust={}

apik = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'
urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&key={}&type={}&keyword={}'.format(lat_ori,long_ori,radius,apik,types_user,keyword_user)
r = requests.get(urls)
data_cust['0'] = r.json()


# # code for next page token

# In[4]:


for number in range(10):
    
    content = str(number)
    if 'next_page_token' in data_cust[content].keys():
        sleep(5)
        pagetoken = data_cust[content]['next_page_token']
        apik = 'AIzaSyC6kM8DKhxMnpesC7fuZrxpU6Cvdmi0DeQ'
        urls = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&type={}&keyword={}&key={}{pagetoken}'.format(lat_ori,long_ori,radius,types_user,keyword_user,apik, pagetoken = "&pagetoken="+pagetoken if pagetoken else "")
        r = requests.get(urls)
        get = requests.post(urls)
        print(get)
        new_id = str(number+1)
        data_cust[new_id] = r.json()
    else:
        print("Done")
        break


# In[5]:


business_status,latitude_loc, longitude_loc,latitude_north, longitude_north,latitude_south, longitude_south, name, place_id, types_places, vicinity = [],[],[],[],[],[],[],[],[],[],[]
price_level,icon,opening,comp,globc,rating,reference,scope,types2,types3,types4,user_rat,photo_height,photo_width,html_attributions,photo_reference=[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]

for i in range(number+1):
    content = str(i)
    for numbers in range(len(data_cust[content]['results'])):
        try: 
            latitude_loc.append(data_cust[content]['results'][numbers]['geometry']['location']['lat'])
        except:
            latitude_loc.append('-')
        try:
            longitude_loc.append(data_cust[content]['results'][numbers]['geometry']['location']['lng'])
        except:
            longitude_loc.append('-')
        try:
            name.append(data_cust[content]['results'][numbers]['name'])
        except:
            name.append('Unknown')
        try:
            place_id.append(data_cust[content]['results'][numbers]['place_id'])
        except:
            place_id.append('-')
        try:
            types_places.append(data_cust[content]['results'][numbers]['types'][0])
        except:
            types_places.append('-')
        try:
            types2.append(data_cust[content]['results'][numbers]['types'][1])
        except:
            types2.append('-')
        try:
            types3.append(data_cust[content]['results'][numbers]['types'][2])
        except:
            types3.append('-')
        try:
            types4.append(data_cust[content]['results'][numbers]['types'][3])
        except:
            types4.append('-')
        try:
            vicinity.append(data_cust[content]['results'][numbers]['vicinity'])
        except:
            vicinity.append('-')
        try:
            business_status.append(data_cust[content]['results'][numbers]['business_status'])
        except:
            business_status.append('-')
        try:
            latitude_north.append(data_cust[content]['results'][numbers]['geometry']['viewport']['northeast']['lat'])
        except:
            latitude_north.append('-')
        try:
            longitude_north.append(data_cust[content]['results'][numbers]['geometry']['viewport']['northeast']['lng'])
        except:
            longitude_north.append('-')
        try:
            latitude_south.append(data_cust[content]['results'][numbers]['geometry']['viewport']['southwest']['lat'])
        except:
            latitude_south.append('-')
        try:
            longitude_south.append(data_cust[content]['results'][numbers]['geometry']['viewport']['southwest']['lng'])
        except:
            longitude_south.append('-')
        try:
            icon.append(data_cust[content]['results'][numbers]['icon'])
        except:
            icon.append('-')
        try:
            comp.append(data_cust[content]['results'][numbers]['plus_code']['compound_code'])
        except:
            comp.append('-')
        try:
            globc.append(data_cust[content]['results'][numbers]['plus_code']['global_code'])
        except:
            globc.append('-')
        try:
            rating.append(data_cust[content]['results'][numbers]['rating'])
        except:
            rating.append('-')
        try:
            reference.append(data_cust[content]['results'][numbers]['reference'])
        except:
            reference.append('-')
        try:
            scope.append(data_cust[content]['results'][numbers]['scope'])
        except:
            scope.append('-')
        try:
            user_rat.append(data_cust[content]['results'][numbers]['user_ratings_total'])
        except:
            user_rat.append('-')
        try:
            opening.append(data_cust[content]['results'][numbers]['opening_hours']['open_now'])
        except:
            opening.append('-')
        try:
            photo_height.append(data_cust[content]['results'][numbers]['photos'][0]['height'])
        except:
            photo_height.append('-')
        try:
            photo_width.append(data_cust[content]['results'][numbers]['photos'][0]['width'])
        except:
            photo_width.append('-')
        try:
            html_attributions.append(data_cust[content]['results'][numbers]['photos'][0]['html_attributions'][0])
        except:
            html_attributions.append('-')
        try:
            photo_reference.append(data_cust[content]['results'][numbers]['photos'][0]['photo_reference'])
        except:
            photo_reference.append('-')
        try:
            price_level.append(data_cust[content]['results'][numbers]['price_level'])
        except:
            price_level.append('-')
            


# In[6]:


print('panjang list business_status: ', len(business_status))
print('panjang list latitude_loc: ', len(latitude_loc))
print('panjang list longitude_loc: ', len(longitude_loc))
print('panjang list latitude_north: ', len(latitude_north))
print('panjang list longitude_north: ', len(longitude_north))
print('panjang list latitude_south: ', len(latitude_south))
print('panjang list longitude_south: ', len(longitude_south))
print('panjang list name: ', len(name))
print('panjang list place_id: ', len(place_id))
print('panjang list address: ', len(vicinity))
print('panjang list icon: ', len(icon))
print('panjang list opening_hours: ', len(opening))
print('panjang list comp: ', len(comp))
print('panjang list globc: ', len(globc))
print('panjang list rating: ', len(rating))
print('panjang list reference: ', len(reference))
print('panjang list scope: ', len(scope))
print('panjang list types2: ', len(types2))
print('panjang list types3: ', len(types3))
print('panjang list types4: ', len(types4))
print('panjang list user_rating: ', len(user_rat))
print('panjang list photo_height: ', len(photo_height))
print('panjang list photo_width: ', len(photo_width))
print('panjang list html_attributions: ', len(html_attributions))
print('panjang list photo_reference: ', len(photo_reference))
print('panjang list price_level: ', len(price_level))


# In[7]:


datacustypes = pd.DataFrame({'business_status':business_status,'customer_name':name,'customer_address':vicinity,'customer_type':types_places,'types_places2':types2,'types_places3':types3,'types_places4':types4,
                             'place_id':place_id,'keyword':keyword_user,'radius':radius,'latitude_origin':lat_ori,'longitude_origin':long_ori,'latitude_destination':latitude_loc,
                            'longitude_destination':longitude_loc,'latitude_north':latitude_north,'longitude_north':longitude_north,'latitude_south':latitude_south,'longitude_south':longitude_south,'icon':icon,
                             'photo_height':photo_height,'photo_width':photo_width,'html_attributions':html_attributions,'photo_reference':photo_reference,
                             'reference':reference,'scope':scope,'opening_hours':opening,'compound_code':comp,'global_code':globc,'price_level':price_level,'rating':rating,'user_rating':user_rat})
pd.set_option('display.max_columns',40)
datacustypes


# # filter business_status = 'operational'

# In[8]:


datacustype = datacustypes.loc[datacustypes['business_status']=='OPERATIONAL']
datacustype


# # phone numb

# In[9]:


data_number = {}
for number in datacustype['place_id'].values:
    apik = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'
    urls = 'https://maps.googleapis.com/maps/api/place/details/json?place_id={}&fields=address_component,adr_address,formatted_address,name,url,utc_offset,formatted_phone_number,international_phone_number,opening_hours,website,review&key={}'.format(number,apik)
    r = requests.get(urls)
    data_number[number] = r.json()


# In[10]:


data_number


# In[11]:


datanumb = pd.DataFrame.from_dict(data_number).T.reset_index()
datanumb.columns = ['place_id','html_attributions','result','status']
datanumb


# In[38]:


datanumb['result'][0]['reviews'][0]


# In[40]:


datanumb['result'][0]['reviews'][1]


# In[12]:


aut1


# In[13]:


name,phone,streetnumb,routel,routes,subcity,city,aal1l,aal1s,aal2,countryl,countrys,postcode= [],[],[],[],[],[],[],[],[],[],[],[],[]
adr_add,formatted_add,intphonenumb,opennow,perd,pert,weekt,url,utc,web=[],[],[],[],[],[],[],[],[],[]
aut1,aut1_url,lan1,pp1,rat1,rtd1,text1,time1=[],[],[],[],[],[],[],[]
aut2,aut2_url,lan2,pp2,rat2,rtd2,text2,time2=[],[],[],[],[],[],[],[]
aut3,aut3_url,lan3,pp3,rat3,rtd3,text3,time3=[],[],[],[],[],[],[],[]
aut4,aut4_url,lan4,pp4,rat4,rtd4,text4,time4=[],[],[],[],[],[],[],[]
aut5,aut5_url,lan5,pp5,rat5,rtd5,text5,time5=[],[],[],[],[],[],[],[]

for number in range(len(datanumb)):
    if datanumb['status'][number] == 'NOT_FOUND':
        name.append('Unknown')
        phone.append(0)
    else:
        try:
            name.append(datanumb['result'][number]['name'])
        except:
            name.append('Unknown')
        try:
            streetnumb.append(datanumb['result'][number]['address_components'][0]['long_name'])
        except:
            streetnumb.append('-')
        try:
            routel.append(datanumb['result'][number]['address_components'][1]['long_name'])
        except:
            routel.append('-')
        try:
            routes.append(datanumb['result'][number]['address_components'][1]['short_name'])
        except:
            routes.append('-')
        try:
            subcity.append(datanumb['result'][number]['address_components'][2]['long_name'])
        except:
            subcity.append('-')
        try:
            city.append(datanumb['result'][number]['address_components'][3]['long_name'])
        except:
            city.append('-')
        try:
            aal2.append(datanumb['result'][number]['address_components'][4]['long_name'])
        except:
            aal2.append('-')
        try:
            aal1l.append(datanumb['result'][number]['address_components'][5]['long_name'])
        except:
            aal1l.append('-')
        try:
            aal1s.append(datanumb['result'][number]['address_components'][5]['short_name'])
        except:
            aal1s.append('-')
        try:
            countryl.append(datanumb['result'][number]['address_components'][6]['long_name'])
        except:
            countryl.append('-')
        try:
            countrys.append(datanumb['result'][number]['address_components'][6]['short_name'])
        except:
            countrys.append('-')
        try:
            postcode.append(datanumb['result'][number]['address_components'][7]['long_name'])
        except:
            postcode.append('-')
        try:
            adr_add.append(datanumb['result'][number]['adr_address'])
        except:
            adr_add.append('-')
        try:
            intphonenumb.append(datanumb['result'][number]['international_phone_number'])
        except:
            intphonenumb.append(0)
        try:
            url.append(datanumb['result'][number]['url'])
        except:
            url.append('-')
        try:
            utc.append(datanumb['result'][number]['utc_offset'])
        except:
            utc.append('-')
        try:
            web.append(datanumb['result'][number]['website'])
        except:
            web.append('-')
        try:
            opennow.append(datanumb['result'][number]['opening_hours']['open_now'])
        except:
            opennow.append('-')
        try:
            perd.append(datanumb['result'][number]['opening_hours']['periods'][0]['open']['day'])
        except:
            perd.append('-')
        try:
            pert.append(datanumb['result'][number]['opening_hours']['periods'][0]['open']['time'])
        except:
            pert.append('-')
        try:
            weekt.append(datanumb['result'][number]['opening_hours']['weekday_text'])
        except:
            weekt.append('-')
        try:
            formatted_add.append(datanumb['result'][number]['formatted_address'])
        except:
            formatted_add.append('Unknown')
        if 'formatted_phone_number' in (datanumb['result'][number].keys()):
            phone.append(datanumb['result'][number]['formatted_phone_number'])
        else:
            phone.append(0)
        try:
            aut1.append(datanumb['result'][number]['reviews'][0]['author_name'])
            aut1_url.append(datanumb['result'][number]['reviews'][0]['author_url'])
            lan1.append(datanumb['result'][number]['reviews'][0]['language'])
            pp1.append(datanumb['result'][number]['reviews'][0]['profile_photo_url'])
            rat1.append(datanumb['result'][number]['reviews'][0]['rating'])
            rtd1.append(datanumb['result'][number]['reviews'][0]['relative_time_description'])
            text1.append(datanumb['result'][number]['reviews'][0]['text'])
            time1.append(datanumb['result'][number]['reviews'][0]['time'])
        except:
            aut1,aut1_url,lan1,pp1,rat1,rtd1,text1,time1.append('-')
        try:
            aut2.append(datanumb['result'][number]['reviews'][1]['author_name'])
            aut2_url.append(datanumb['result'][number]['reviews'][1]['author_url'])
            lan2.append(datanumb['result'][number]['reviews'][1]['language'])
            pp2.append(datanumb['result'][number]['reviews'][1]['profile_photo_url'])
            rat2.append(datanumb['result'][number]['reviews'][1]['rating'])
            rtd2.append(datanumb['result'][number]['reviews'][1]['relative_time_description'])
            text2.append(datanumb['result'][number]['reviews'][1]['text'])
            time2.append(datanumb['result'][number]['reviews'][1]['time'])
        except:
            aut2,aut2_url,lan2,pp2,rat2,rtd2,text2,time2.append('-')
        try:
            aut3.append(datanumb['result'][number]['reviews'][2]['author_name'])
            aut3_url.append(datanumb['result'][number]['reviews'][2]['author_url'])
            lan3.append(datanumb['result'][number]['reviews'][2]['language'])
            pp3.append(datanumb['result'][number]['reviews'][2]['profile_photo_url'])
            rat3.append(datanumb['result'][number]['reviews'][2]['rating'])
            rtd3.append(datanumb['result'][number]['reviews'][2]['relative_time_description'])
            text3.append(datanumb['result'][number]['reviews'][2]['text'])
            time3.append(datanumb['result'][number]['reviews'][2]['time'])
        except:
            aut3,aut3_url,lan3,pp3,rat3,rtd3,text3,time3.append('-')
        try:
            aut4.append(datanumb['result'][number]['reviews'][3]['author_name'])
            aut4_url.append(datanumb['result'][number]['reviews'][3]['author_url'])
            lan4.append(datanumb['result'][number]['reviews'][3]['language'])
            pp4.append(datanumb['result'][number]['reviews'][3]['profile_photo_url'])
            rat4.append(datanumb['result'][number]['reviews'][3]['rating'])
            rtd4.append(datanumb['result'][number]['reviews'][3]['relative_time_description'])
            text4.append(datanumb['result'][number]['reviews'][3]['text'])
            time4.append(datanumb['result'][number]['reviews'][3]['time'])
        except:
            aut4,aut4_url,lan4,pp4,rat4,rtd4,text4,time4.append('-')
        try:
            aut5.append(datanumb['result'][number]['reviews'][4]['author_name'])
            aut5_url.append(datanumb['result'][number]['reviews'][4]['author_url'])
            lan5.append(datanumb['result'][number]['reviews'][4]['language'])
            pp5.append(datanumb['result'][number]['reviews'][4]['profile_photo_url'])
            rat5.append(datanumb['result'][number]['reviews'][4]['rating'])
            rtd5.append(datanumb['result'][number]['reviews'][4]['relative_time_description'])
            text5.append(datanumb['result'][number]['reviews'][4]['text'])
            time5.append(datanumb['result'][number]['reviews'][4]['time'])
        except:
            aut5,aut5_url,lan5,pp5,rat5,rtd5,text5,time5.append('-')


# In[14]:


aut1


# In[15]:


aut2


# In[16]:


aut3


# In[17]:


datanumb2 = pd.DataFrame({'customer_name':name,'phone_number':phone,'formatted_address':formatted_add,'street_number':streetnumb,'route_long_name':routel,
                         'route_short_name':routes,'subcity':subcity,'city':city,'administrative_area_level_2':aal2,'administrative_area_level_1_ln':aal1l,
                         'administrative_area_level_1_sn':aal1s,'country_ln':countryl,'country_sn':countrys,'postal_code':postcode,'adr_address':adr_add,
                         'international_phone_numb':intphonenumb,'open_now':opennow,'periods_day':perd,'periods_time':pert,'week_day':weekt,'url':url,
                         'utc-offset':utc,'website':web,
                          'author_name_1':aut1,'author_url_1':aut1_url,'language_1':lan1,'photo_profile_url_1':pp1,'rating_1':rat1,'relative_time_description_1':rtd1,'text_1':text1,'time_1':time1,
                         'author_name_2':aut2,'author_url_2':aut2_url,'language_2':lan2,'photo_profile_url_2':pp2,'rating_2':rat2,'relative_time_description_2':rtd2,'text_2':text2,'time_2':time2,
                         'author_name_3':aut3,'author_url_3':aut3_url,'language_3':lan3,'photo_profile_url_3':pp3,'rating_3':rat3,'relative_time_description_3':rtd3,'text_3':text3,'time_3':time3,
                         'author_name_4':aut4,'author_url_4':aut4_url,'language_4':lan4,'photo_profile_url_4':pp4,'rating_4':rat4,'relative_time_description_4':rtd4,'text_4':text4,'time_4':time4,
                         'author_name_5':aut5,'author_url_5':aut5_url,'language_5':lan5,'photo_profile_url_5':pp5,'rating_5':rat5,'relative_time_description_5':rtd5,'text_5':text5,'time_5':time5})
datanumb2['place_id'] = datanumb['place_id']
pd.set_option('display.max_columns',100)
datanumb2


# # merge

# In[18]:


datamerge=datacustype.merge(datanumb2, how='left', on='place_id')
datamerge


# # dummy

# In[15]:


datadummy=datamerge.copy()
datadummy


# In[16]:


datadummydrop=datadummy.drop(['customer_name_y'], axis = 1)
datadummydrop.rename(columns={'customer_name_x': 'customer_name'}, inplace=True)
datadummydrop


# # distance matrix

# In[17]:


import pandas as pd
import googlemaps


# In[18]:


API_key = 'AIzaSyDiFSOQvPbWVh3voJPSSORT9TSfKAXMy7E'#enter Google Maps API key
gmaps = googlemaps.Client(key=API_key)


# In[19]:


distancedrive = []

# Loop through each row in the data frame using pairwise
for number in range(datadummydrop.shape[0]):
      #Assign latitude and longitude as origin/departure points
    LatOrigin = datadummydrop['latitude_origin'][number]
    LongOrigin = datadummydrop['longitude_origin'][number]
    origins = (LatOrigin,LongOrigin)

      #Assign latitude and longitude from the next row as the destination point
    LatDest = datadummydrop['latitude_destination'][number]  # Save value as lat
    LongDest = datadummydrop['longitude_destination'][number] # Save value as lat
    destination = (LatDest,LongDest)
    

      #pass origin and destination variables to distance_matrix function# output in meters
    result = gmaps.distance_matrix(origins, destination, mode='driving',avoid='tolls',units='metric',traffic_model='optimistic',departure_time=1703981100)["rows"][0]["elements"][0]["distance"]["value"]
 #1703981100    #1606867500 
      #append result to list
    distancedrive.append(result)


# In[21]:


datadummydrop['Distance_to_Infra(m)']=distancedrive
datadummydrop3=datadummydrop.sort_values(by=['Distance_to_Infra(m)'],ascending=True,ignore_index=True)
datadummydrop3


# In[ ]:


datadummydrop3


# # dataframe to postgre

# In[134]:


import psycopg2

database = psycopg2.connect (database = "coba", 
                             user="postgres", 
                             password="kasihtaunggakya?", 
                             host="localhost")

cursor = database.cursor()

for i in datadummydrop3.index:
    c1 = datadummydrop3['customer_name'][i]
    c2 = datadummydrop3['customer_address'][i]
    c3 = datadummydrop3['customer_type'][i]
    c4 = datadummydrop3['keyword'][i]
    c5 = datadummydrop3['radius'][i]
    c6 = datadummydrop3['place_id'][i]
    c7 = datadummydrop3['latitude_origin'][i]
    c8 = datadummydrop3['longitude_origin'][i]
    c9 = datadummydrop3['latitude_destination'][i]
    c10 = datadummydrop3['longitude_destination'][i]
    c11 = datadummydrop3['phone_number'][i]
    c12 = datadummydrop3['Distance_to_Infra_driving'][i]
    c13 = datadummydrop3['Distance_to_Infra_walking'][i]
    query = """
    Insert into sukses3(customer_name, customer_address, customer_type, keyword, radius, place_id, latitude_origin, longitude_origin, latitude_destination, longitude_destination, phone_number, Distance_to_Infra_driving, Distance_to_Infra_walking) VALUES('%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,'%s',%s,%s);
    """ % (c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13)
    cursor.execute(query)
cursor.close()
    
database.commit()
database.close()

print ("Data berhasil di upload")

